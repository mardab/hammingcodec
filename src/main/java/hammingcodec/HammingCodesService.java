package hammingcodec;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class HammingCodesService {

	/*
	 * /home/helper/Desktop/Tele/apple.jpg
	 * /home/helper/Desktop/Tele/encodedApple
	 * /home/helper/Desktop/Tele/decodedApple.jpg
	 * 
	*/
	
	public static void main(String[] args)  {
		// TODO Auto-generated method stub
		HammingCodesService codeService = new HammingCodesService();

		Scanner scanner = new Scanner(System.in);
		
		String pathToFileToEncode=null;
		String pathToFileToDecode=null;
		String pathToTargetFile=null;
		int selectedCase = 0;

		System.out.println("1-zakoduj, 2-zdekoduj");
	
		
		while (true) {
			
			try {
				selectedCase = scanner.nextInt();
			} catch (Exception e) {
				System.exit(1);
			}
			
			switch (selectedCase) {

			case 1:
				System.out.println("Podaj ścieżke do pliku do zakodowania:");
				pathToFileToEncode = scanner.next();

				System.out.println("Podaj ścieżke do pliku docelowego:");
				pathToTargetFile = scanner.next();

				HammingCodesService.encode(pathToFileToEncode, pathToTargetFile);
				
				System.exit(0);
				break;

			case 2:

				System.out.println("Podaj ścieżke do pliku do zdekodowania:");
				pathToFileToDecode = scanner.next();

				System.out.println("Podaj ścieżke do pliku docelowego:");
				pathToTargetFile = scanner.next();

				HammingCodesService.decode(pathToFileToDecode, pathToTargetFile);
				
				System.exit(0);
				break;

			default:
				System.exit(0);
				break;
			}
		}


	}

	public HammingCodesService() {

		hammingMatrixBitByBit = new byte[][] { { 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0 },
				{ 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0 }, { 1, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0 },
				{ 0, 0, 1, 0, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0 }, { 0, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0 },
				{ 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0 }, { 0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0 },
				{ 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1 } };

		hammingMatrixInBytes = new byte[][] { { (byte) 0b11111110, (byte) 0b10000000 },
				{ (byte) 0b11111001, (byte) 0b01000000 }, { (byte) 0b11100101, (byte) 0b00100000 },
				{ (byte) 0b00101011, (byte) 0b00010000 }, { (byte) 0b00110101, (byte) 0b00001000 },
				{ (byte) 0b01001101, (byte) 0b00000100 }, { (byte) 0b01010111, (byte) 0b00000010 },
				{ (byte) 0b10001111, (byte) 0b00000001 }

		};

	}

	private static byte[][] hammingMatrixBitByBit;
	public static byte[][] hammingMatrixInBytes;
	private static int[][] hammingMatrixInSignedIntRepresentation;

	public static void encode(String pathToInputFile, String pathToOutputFile) {

		writeBytesToFile(encodeMessage(readFileToBytes(pathToInputFile)), pathToOutputFile);

	}

	private static void decode(String pathToInputFile, String pathToOutputFile) {

		// troche nieczytelne?
		writeBytesToFile(decodeByte(rewriteBytesToHammingNotationArray(readFileToBytes(pathToInputFile))),
				pathToOutputFile);

	}

	private static byte[][] rewriteBytesToHammingNotationArray(byte[] encodedMessageByteByByte) {
		byte[][] encodedMessageInHammingNotation = new byte[encodedMessageByteByByte.length / 2][2];

		int arrayIterator = 0;

		for (int byteNumber = 0; byteNumber < encodedMessageByteByByte.length; byteNumber++) {
			if (byteNumber % 2 == 0) {

				encodedMessageInHammingNotation[byteNumber / 2][0] = encodedMessageByteByByte[byteNumber];

			} else {
				encodedMessageInHammingNotation[byteNumber / 2][1] = encodedMessageByteByByte[byteNumber];
			}
		}

		return encodedMessageInHammingNotation;
	}

	private static byte[] readFileToBytes(String pathToInputFile) {

		byte[] fileContent = null;

		try {
			fileContent = Files.readAllBytes(Paths.get(pathToInputFile));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			System.out.println("Nie znaleziono pliku do wczytania. Program przerwany.");
			System.exit(1);
		}

		return fileContent;
	}

	private static void writeBytesToFile(byte[] message, String fileName) {

		File fileToWrite = new File(fileName);
		if (!fileToWrite.exists()) {
			try {
				fileToWrite.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				System.out.println("Nie udalo sie utworzyc pliku. Program przerwany.");
				System.exit(1);
			}
		}

		DataOutputStream dataOutputStream;
		try {
			dataOutputStream = new DataOutputStream(new FileOutputStream(fileName));

			for (int byteNumber = 0; byteNumber < message.length; byteNumber++) {
				dataOutputStream.writeByte(message[byteNumber]);
			}
			dataOutputStream.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Nie udalo sie zapisac pliku. Program przerwany.");
			System.exit(1);
		}

	}

	private static byte[] encodeMessage(byte[] originalMessage) {

		byte[] encodedMessage = new byte[originalMessage.length * 2];

		int encodedMessageIterator = 0;
		int sumOfBits = 0;

		byte parityTempByte = (byte) 0b00000000;
		;
		byte parityByte = (byte) 0b00000000;

		for (int byteOfMessageNumber = 0; byteOfMessageNumber < originalMessage.length; byteOfMessageNumber++) {

			encodedMessage[encodedMessageIterator] = originalMessage[byteOfMessageNumber];
			encodedMessageIterator++;

			for (int rowOfHammingMatrixNumber = 0; rowOfHammingMatrixNumber < HammingCodesService.hammingMatrixInBytes.length; rowOfHammingMatrixNumber++) {

				parityTempByte = (byte) (HammingCodesService.hammingMatrixInBytes[rowOfHammingMatrixNumber][0]
						& originalMessage[byteOfMessageNumber]);

				for (int bitPosition = 0; bitPosition < 8; bitPosition++) {
					sumOfBits += (int) parityTempByte >> bitPosition & 1;
				}

				parityByte = (byte) (parityByte << 1 | (byte) (sumOfBits % 2));

				sumOfBits = 0;

			}

			encodedMessage[encodedMessageIterator] = parityByte;
			encodedMessageIterator++;

		}

		return encodedMessage;
	}

	private static byte[] decodeByte(byte[][] encodedMessage) {

		byte[] decodedMessage = new byte[encodedMessage.length];

		for (int i = 0; i < encodedMessage.length; i++) {

			byte errorCheckCode = calculateErrorCheckCode(encodedMessage[i]);

			if (errorCheckCode == 0b00000000) {
				decodedMessage[i] = encodedMessage[i][0];
			} else {
				decodedMessage[i] = correctError(encodedMessage[i], errorCheckCode);
			}
		}

		return decodedMessage;

	}

	private static byte correctError(byte[] encodedMessage, byte errorCheckCode) {

		byte decodedByte = (byte) 0b0;
		Integer singleErrorPostion = findSingleErrorPosition(errorCheckCode);

		int[] doubleErrorPositions = null;

		if (singleErrorPostion == null) {
			doubleErrorPositions = findDoubleErrorPosition(errorCheckCode);

			if (doubleErrorPositions == null) {
				System.out.println("Wiadomość jest uszkodzona, przerwano działanie programu.");
				System.exit(1);

			} else {
				if (doubleErrorPositions[0]<8 || doubleErrorPositions[1]<8) {

					if (doubleErrorPositions[0] < 8) {
						decodedByte = shiftBitOnPosition(encodedMessage[0], doubleErrorPositions[0]);
					}

					// if (Integer.min(doubleErrorPositions[0],
					// doubleErrorPositions[1]) < 8) {
					if (doubleErrorPositions[1] < 8) {
						decodedByte = shiftBitOnPosition(decodedByte, doubleErrorPositions[1]);
					}
				}

				else {
					decodedByte = encodedMessage[0];
				}

			}

		} else if (singleErrorPostion < 8) {
			decodedByte = shiftBitOnPosition(encodedMessage[0], singleErrorPostion);
		} else if (singleErrorPostion >= 8) {
			decodedByte = encodedMessage[0];
		}

		return decodedByte;
	}

	private static int[] findDoubleErrorPosition(byte errorCheckCode) {

		byte[] errorCheckCodeBitByBit = rewriteErrorCheckByteToBitsArray(errorCheckCode);

		int hammingMatrixRowNumber = 0;

		for (int hammingMatrixFirstBitColumn = 0; hammingMatrixFirstBitColumn < 15; hammingMatrixFirstBitColumn++) {
			for (int hammingMatrixSecondBitColumn = hammingMatrixFirstBitColumn
					+ 1; hammingMatrixSecondBitColumn < 16; hammingMatrixSecondBitColumn++) {

				while ((HammingCodesService.hammingMatrixBitByBit[hammingMatrixRowNumber][hammingMatrixFirstBitColumn]
						+ HammingCodesService.hammingMatrixBitByBit[hammingMatrixRowNumber][hammingMatrixSecondBitColumn])
						% 2 == errorCheckCodeBitByBit[hammingMatrixRowNumber]) {
					hammingMatrixRowNumber++;
					if (hammingMatrixRowNumber == 8) {
						return new int[] { hammingMatrixFirstBitColumn, hammingMatrixSecondBitColumn };
					}
				}
				hammingMatrixRowNumber = 0;
			}
		}
		return null;
	}

	private static byte shiftBitOnPosition(byte message, int[] doubleErrorPositions) {

		for (int i = 0; i < doubleErrorPositions.length; i++) {
			if (doubleErrorPositions[i] < 8) {
				message = shiftBitOnPosition(message, doubleErrorPositions[i]);
			}
		}

		return message;
	}

	private static byte shiftBitOnPosition(byte message, Integer errorPosition) {

		byte shiftedByte = (byte) 0b0;

		for (int i = 0; i < 8; i++) {
			if (i == errorPosition) {
				if ((byte) (message >> (7 - i) & 1) == 0b1) {
					shiftedByte = (byte) ((byte) (shiftedByte << 1) | (byte) 0b0);
				} else {
					shiftedByte = (byte) ((byte) (shiftedByte << 1) | (byte) 0b1);
				}

			} else {
				shiftedByte = (byte) ((byte) (shiftedByte << 1) | (byte) (message >> (7 - i) & 1));
			}
		}

		return shiftedByte;
	}

	private static Integer findSingleErrorPosition(byte errorCheckCode) {

		byte[] errorCheckCodeBitByBit = rewriteErrorCheckByteToBitsArray(errorCheckCode);

		int j = 0;

		for (int hammingMatrixColumn = 0; hammingMatrixColumn < 16; hammingMatrixColumn++) {

			while (HammingCodesService.hammingMatrixBitByBit[j][hammingMatrixColumn] == errorCheckCodeBitByBit[j]) {

				j++;

				if (j == 8)
					return Integer.valueOf(hammingMatrixColumn);

			}

			j = 0;
		}

		return null;
	}

	private static byte[] rewriteErrorCheckByteToBitsArray(byte errorCheckCode) {
		byte[] errorCheckCodeBitByBit = new byte[8];

		for (int i = 0; i < errorCheckCodeBitByBit.length; i++) {
			errorCheckCodeBitByBit[i] = (byte) (errorCheckCode >> 7 - i & 1);
		}

		return errorCheckCodeBitByBit;
	}

	private static byte calculateErrorCheckCode(byte[] encodedBytes) {

		int sumOfBits = 0;

		byte messageTempByte;
		byte parityTempByte;

		byte errorCheckByte = (byte) 0b00000000;

		for (int numberOfHammingMatrixRow = 0; numberOfHammingMatrixRow < hammingMatrixInBytes.length; numberOfHammingMatrixRow++) {

			sumOfBits = 0;

			messageTempByte = (byte) (encodedBytes[0] & hammingMatrixInBytes[numberOfHammingMatrixRow][0]);
			parityTempByte = (byte) (encodedBytes[1] & hammingMatrixInBytes[numberOfHammingMatrixRow][1]);

			for (int bitPosition = 0; bitPosition < 8; bitPosition++) {
				sumOfBits += (int) messageTempByte >> bitPosition & 1;
				sumOfBits += (int) parityTempByte >> bitPosition & 1;
			}

			errorCheckByte = (byte) (errorCheckByte << 1 | (byte) (sumOfBits % 2));

		}
		return errorCheckByte;
	}
}
